"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.transformAreaOrigin = exports.transformArea = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _pannable = require("./pannable.js");

var _promise = require("./promise.js");

var _transform = require("./transform.js");

var _zoomable = require("./zoomable.js");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var container = document.getElementById('container');
var area = container.firstElementChild;
var step = .4;
var _transformation = {};

var listenEvents = function listenEvents() {
  var unzoom;
  container.addEventListener('wheel', /*#__PURE__*/function () {
    var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(_ref) {
      var pageX, pageY, deltaY, deltaX, style, _style$transition, transition, delta;

      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              pageX = _ref.pageX, pageY = _ref.pageY, deltaY = _ref.deltaY, deltaX = _ref.deltaX;

              if (!unzoom) {
                _context.next = 3;
                break;
              }

              return _context.abrupt("return");

            case 3:
              style = area.style;
              _style$transition = style.transition, transition = _style$transition === void 0 ? null : _style$transition;
              style.transition = 'none';
              _context.next = 8;
              return (0, _promise.waitNextTick)();

            case 8:
              // Normalize to deltaX in case shift modifier is used on Mac
              delta = deltaY === 0 && deltaX ? deltaX : deltaY;
              _context.next = 11;
              return zoom({
                x: pageX,
                y: pageY,
                ratio: Math.exp((delta < 0 ? 1 : -1) * step / 3)
              });

            case 11:
              style.transition = transition;

            case 12:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function (_x) {
      return _ref2.apply(this, arguments);
    };
  }());
  container.addEventListener('mousemove', /*#__PURE__*/function () {
    var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(event) {
      var style, _style$transition2, transition;

      return _regenerator["default"].wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              if (!(unzoom || event.buttons !== 1)) {
                _context2.next = 2;
                break;
              }

              return _context2.abrupt("return");

            case 2:
              event.preventDefault();
              style = area.style;
              _style$transition2 = style.transition, transition = _style$transition2 === void 0 ? null : _style$transition2;
              style.transition = 'none';
              _context2.next = 8;
              return (0, _promise.waitNextTick)();

            case 8:
              _context2.next = 10;
              return panBy({
                deltaX: event.movementX,
                deltaY: event.movementY
              });

            case 10:
              style.transition = transition;

            case 11:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function (_x2) {
      return _ref3.apply(this, arguments);
    };
  }());
  container.addEventListener('dblclick', /*#__PURE__*/function () {
    var _ref5 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(_ref4) {
      var target, containerWidth, offsetTop, offsetLeft, _target$getBoundingCl, width, ratio, scale;

      return _regenerator["default"].wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              target = _ref4.target;

              if (!unzoom) {
                _context4.next = 3;
                break;
              }

              return _context4.abrupt("return", unzoom());

            case 3:
              unzoom = /*#__PURE__*/function () {
                var _ref6 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3() {
                  return _regenerator["default"].wrap(function _callee3$(_context3) {
                    while (1) {
                      switch (_context3.prev = _context3.next) {
                        case 0:
                          _context3.t0 = Object;
                          _context3.t1 = _transformation;
                          _context3.next = 4;
                          return transformArea();

                        case 4:
                          _context3.t2 = _context3.sent;

                          _context3.t0.assign.call(_context3.t0, _context3.t1, _context3.t2);

                          unzoom = undefined;

                        case 7:
                        case "end":
                          return _context3.stop();
                      }
                    }
                  }, _callee3);
                }));

                return function unzoom() {
                  return _ref6.apply(this, arguments);
                };
              }();

              containerWidth = container.clientWidth;
              offsetTop = target.offsetTop, offsetLeft = target.offsetLeft;
              _target$getBoundingCl = target.getBoundingClientRect(), width = _target$getBoundingCl.width;
              ratio = containerWidth / width;
              scale = _transformation.scale * ratio;
              _context4.t0 = Object;
              _context4.t1 = _transformation;
              _context4.next = 13;
              return transformAreaOrigin(_objectSpread(_objectSpread({}, _transformation), {}, {
                newOriginX: 0,
                newOriginY: 0
              }));

            case 13:
              _context4.t2 = _context4.sent;

              _context4.t0.assign.call(_context4.t0, _context4.t1, _context4.t2);

              _context4.t3 = Object;
              _context4.t4 = _transformation;
              _context4.next = 19;
              return transformArea(_objectSpread(_objectSpread({}, _transformation), {}, {
                scale: scale,
                translateX: -(offsetLeft * scale),
                translateY: -(offsetTop * scale)
              }));

            case 19:
              _context4.t5 = _context4.sent;

              _context4.t3.assign.call(_context4.t3, _context4.t4, _context4.t5);

            case 21:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4);
    }));

    return function (_x3) {
      return _ref5.apply(this, arguments);
    };
  }());
};

var transformArea = (0, _transform.transform)(area);
exports.transformArea = transformArea;
var transformAreaOrigin = (0, _transform.transformOrigin)(area);
exports.transformAreaOrigin = transformAreaOrigin;

var _Zoomable = (0, _zoomable.Zoomable)({
  element: area,
  transformation: _transformation,
  transformElement: transformArea,
  transformElementOrigin: transformAreaOrigin
}),
    zoom = _Zoomable.zoom;

var _Pannable = (0, _pannable.Pannable)({
  element: area,
  transformation: _transformation,
  transformElement: transformArea,
  transformElementOrigin: transformAreaOrigin
}),
    panBy = _Pannable.panBy; // eslint-disable-next-line no-unused-vars


var start1 = /*#__PURE__*/function () {
  var _ref7 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5() {
    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.t0 = Object;
            _context5.t1 = _transformation;
            _context5.next = 4;
            return transformArea({
              animated: false
            });

          case 4:
            _context5.t2 = _context5.sent;

            _context5.t0.assign.call(_context5.t0, _context5.t1, _context5.t2);

            _context5.t3 = Object;
            _context5.t4 = _transformation;
            _context5.next = 10;
            return transformArea({
              originX: 50,
              originY: 50,
              translateX: 100,
              translateY: 100,
              scale: 1.752
            });

          case 10:
            _context5.t5 = _context5.sent;

            _context5.t3.assign.call(_context5.t3, _context5.t4, _context5.t5);

            _context5.t6 = Object;
            _context5.t7 = _transformation;
            _context5.next = 16;
            return transformAreaOrigin(_objectSpread(_objectSpread({}, _transformation), {}, {
              newOriginX: 0,
              newOriginY: 100
            }));

          case 16:
            _context5.t8 = _context5.sent;

            _context5.t6.assign.call(_context5.t6, _context5.t7, _context5.t8);

            _context5.t9 = Object;
            _context5.t10 = _transformation;
            _context5.next = 22;
            return transformArea(_objectSpread(_objectSpread({}, _transformation), {}, {
              scale: 2
            }));

          case 22:
            _context5.t11 = _context5.sent;

            _context5.t9.assign.call(_context5.t9, _context5.t10, _context5.t11);

            listenEvents();

          case 25:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function start1() {
    return _ref7.apply(this, arguments);
  };
}(); // eslint-disable-next-line no-unused-vars


var start2 = /*#__PURE__*/function () {
  var _ref8 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6() {
    return _regenerator["default"].wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.t0 = Object;
            _context6.t1 = _transformation;
            _context6.next = 4;
            return transformArea({
              animated: false
            });

          case 4:
            _context6.t2 = _context6.sent;

            _context6.t0.assign.call(_context6.t0, _context6.t1, _context6.t2);

            _context6.next = 8;
            return zoom({
              x: 400,
              y: 200,
              ratio: .25
            });

          case 8:
            _context6.next = 10;
            return zoom({
              x: 400,
              y: 200,
              ratio: 2
            });

          case 10:
            _context6.next = 12;
            return zoom({
              x: 400,
              y: 200,
              ratio: 2
            });

          case 12:
            listenEvents();

          case 13:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));

  return function start2() {
    return _ref8.apply(this, arguments);
  };
}(); // eslint-disable-next-line no-unused-vars


var start = /*#__PURE__*/function () {
  var _ref9 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7() {
    return _regenerator["default"].wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.t0 = Object;
            _context7.t1 = _transformation;
            _context7.next = 4;
            return transformArea({
              animated: false
            });

          case 4:
            _context7.t2 = _context7.sent;

            _context7.t0.assign.call(_context7.t0, _context7.t1, _context7.t2);

            listenEvents();

          case 7:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));

  return function start() {
    return _ref9.apply(this, arguments);
  };
}();

start2();