"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Zoomable = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _transform = require("./transform.js");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var Zoomable = function Zoomable(_ref) {
  var element = _ref.element,
      _ref$transformation = _ref.transformation,
      transformation = _ref$transformation === void 0 ? {} : _ref$transformation,
      _ref$transformElement = _ref.transformElement,
      transformElement = _ref$transformElement === void 0 ? (0, _transform.transform)(element) : _ref$transformElement,
      _ref$transformElement2 = _ref.transformElementOrigin,
      transformElementOrigin = _ref$transformElement2 === void 0 ? (0, _transform.transformOrigin)(element) : _ref$transformElement2;

  var zoom = /*#__PURE__*/function () {
    var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(_ref2) {
      var x, y, _ref2$ratio, ratio, _ref2$scale, newScale, scale, _element$getBoundingC, left, top;

      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              x = _ref2.x, y = _ref2.y, _ref2$ratio = _ref2.ratio, ratio = _ref2$ratio === void 0 ? 1 : _ref2$ratio, _ref2$scale = _ref2.scale, newScale = _ref2$scale === void 0 ? transformation.scale * ratio : _ref2$scale;
              scale = transformation.scale;
              _element$getBoundingC = element.getBoundingClientRect(), left = _element$getBoundingC.left, top = _element$getBoundingC.top;
              _context.t0 = Object;
              _context.t1 = transformation;
              _context.next = 7;
              return transformElementOrigin(_objectSpread(_objectSpread({}, transformation), {}, {
                newOriginX: (x - left) / scale,
                newOriginY: (y - top) / scale
              }));

            case 7:
              _context.t2 = _context.sent;

              _context.t0.assign.call(_context.t0, _context.t1, _context.t2);

              _context.t3 = Object;
              _context.t4 = transformation;
              _context.next = 13;
              return transformElement(_objectSpread(_objectSpread({}, transformation), {}, {
                scale: newScale
              }));

            case 13:
              _context.t5 = _context.sent;

              _context.t3.assign.call(_context.t3, _context.t4, _context.t5);

            case 15:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function zoom(_x) {
      return _ref3.apply(this, arguments);
    };
  }();

  return {
    zoom: zoom
  };
};

exports.Zoomable = Zoomable;