"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.transformOrigin = exports.changeOrigin = exports.transform = exports.hasTransition = exports.waitTransitionEnd = exports.renderMatrix = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _promise = require("./promise.js");

var renderMatrix = function renderMatrix(_ref) {
  var _ref$scale = _ref.scale,
      scale = _ref$scale === void 0 ? 1 : _ref$scale,
      _ref$translate = _ref.translate,
      translate = _ref$translate === void 0 ? 0 : _ref$translate,
      _ref$translateX = _ref.translateX,
      translateX = _ref$translateX === void 0 ? translate : _ref$translateX,
      _ref$translateY = _ref.translateY,
      translateY = _ref$translateY === void 0 ? translate : _ref$translateY;
  return "matrix(".concat(scale, ", 0, 0, ").concat(scale, ", ").concat(translateX, ", ").concat(translateY, ")");
};

exports.renderMatrix = renderMatrix;

var waitTransitionEnd = function waitTransitionEnd(element) {
  return new Promise(function (resolve) {
    var onTransitionEnd = function onTransitionEnd() {
      element.removeEventListener('transitionend', onTransitionEnd);
      resolve();
    };

    element.addEventListener('transitionend', onTransitionEnd);
  });
};

exports.waitTransitionEnd = waitTransitionEnd;

var hasTransition = function hasTransition(element) {
  var style = window.getComputedStyle(element);
  return style.getPropertyValue('transition-duration') !== '0s' && style.getPropertyValue('transition-property') !== 'none';
};

exports.hasTransition = hasTransition;

var transform = function transform(element) {
  return function () {
    var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref2$originX = _ref2.originX,
        originX = _ref2$originX === void 0 ? 0 : _ref2$originX,
        _ref2$originY = _ref2.originY,
        originY = _ref2$originY === void 0 ? 0 : _ref2$originY,
        _ref2$translateX = _ref2.translateX,
        translateX = _ref2$translateX === void 0 ? 0 : _ref2$translateX,
        _ref2$translateY = _ref2.translateY,
        translateY = _ref2$translateY === void 0 ? 0 : _ref2$translateY,
        _ref2$scale = _ref2.scale,
        scale = _ref2$scale === void 0 ? 1 : _ref2$scale,
        _ref2$animated = _ref2.animated,
        animated = _ref2$animated === void 0 ? true : _ref2$animated;

    var style = element.style;
    var matrix = {
      originX: originX,
      originY: originY,
      translateX: translateX,
      translateY: translateY,
      scale: scale
    };
    style.transformOrigin = "".concat(originX, "px ").concat(originY, "px");
    style.transform = renderMatrix(matrix);
    var wait = animated && hasTransition(element) ? waitTransitionEnd : _promise.waitNextTick;
    return wait(element).then(function () {
      return matrix;
    });
  };
};

exports.transform = transform;

var changeOrigin = function changeOrigin(_ref3) {
  var newOriginX = _ref3.newOriginX,
      originX = _ref3.originX,
      newOriginY = _ref3.newOriginY,
      originY = _ref3.originY,
      scale = _ref3.scale,
      translateX = _ref3.translateX,
      translateY = _ref3.translateY;
  var deltaX = newOriginX - originX;
  var deltaY = newOriginY - originY;
  return {
    originX: newOriginX,
    originY: newOriginY,
    translateX: translateX + deltaX * (scale - 1),
    translateY: translateY + deltaY * (scale - 1),
    scale: scale
  };
};

exports.changeOrigin = changeOrigin;

var transformOrigin = function transformOrigin(element) {
  return /*#__PURE__*/function () {
    var _ref4 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(transformation) {
      var style, _style$transition, transition, newTransformation;

      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              style = element.style;
              _style$transition = style.transition, transition = _style$transition === void 0 ? null : _style$transition;
              style.transition = 'none';
              _context.next = 5;
              return (0, _promise.waitNextTick)();

            case 5:
              newTransformation = changeOrigin(transformation);
              _context.next = 8;
              return transform(element)(newTransformation);

            case 8:
              style.transition = transition;
              return _context.abrupt("return", newTransformation);

            case 10:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function (_x) {
      return _ref4.apply(this, arguments);
    };
  }();
};

exports.transformOrigin = transformOrigin;