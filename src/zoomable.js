import { transform, transformOrigin } from './transform.js'





export const Zoomable = ({
  element,
  transformation = {},
  transformElement = transform(element),
  transformElementOrigin = transformOrigin(element),
}) => {

  const zoom = async ({ x, y, ratio = 1, scale: newScale = transformation.scale * ratio }) => {

    const { scale } = transformation

    const { left, top } = element.getBoundingClientRect()

    Object.assign(transformation, await transformElementOrigin({
      ...transformation,
      newOriginX: (x - left) / scale,
      newOriginY: (y - top) / scale,
    }))

    Object.assign(transformation, await transformElement({
      ...transformation,
      scale: newScale,
    }))
  }

  return {
    zoom
  }
}
