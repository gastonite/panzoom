import { waitNextTick } from './promise.js'



export const renderMatrix = ({
  scale = 1,
  translate = 0,
  translateX = translate,
  translateY = translate
}) => {

  return `matrix(${scale}, 0, 0, ${scale}, ${translateX}, ${translateY})`
}

export const waitTransitionEnd = element => new Promise(resolve => {

  const onTransitionEnd = () => {

    element.removeEventListener('transitionend', onTransitionEnd)

    resolve()
  }

  element.addEventListener('transitionend', onTransitionEnd)
})

export const hasTransition = element => {

  const style = window.getComputedStyle(element)

  return style.getPropertyValue('transition-duration') !== '0s'
    && style.getPropertyValue('transition-property') !== 'none'
}

export const transform = element => ({
  originX = 0,
  originY = 0,
  translateX = 0,
  translateY = 0,
  scale = 1,
  animated = true,
} = {}) => {

  const { style } = element

  const matrix = {
    originX,
    originY,
    translateX,
    translateY,
    scale,
  }

  style.transformOrigin = `${originX}px ${originY}px`
  style.transform = renderMatrix(matrix)

  const wait = animated && hasTransition(element)
    ? waitTransitionEnd
    : waitNextTick

  return wait(element).then(() => matrix)
}

export const changeOrigin = ({
  newOriginX,
  originX,
  newOriginY,
  originY,
  scale,
  translateX,
  translateY
}) => {

  const deltaX = newOriginX - originX
  const deltaY = newOriginY - originY

  return {
    originX: newOriginX,
    originY: newOriginY,
    translateX: translateX + deltaX * (scale - 1),
    translateY: translateY + deltaY * (scale - 1),
    scale
  }
}

export const transformOrigin = element => async transformation => {

  const { style } = element
  const { transition = null } = style

  style.transition = 'none'
  await waitNextTick()

  const newTransformation = changeOrigin(transformation)

  await transform(element)(newTransformation)

  style.transition = transition

  return newTransformation
}

