import { Pannable } from './pannable.js'
import { waitNextTick } from './promise.js'
import { transform, transformOrigin } from './transform.js'
import { Zoomable } from './zoomable.js'



const container = document.getElementById('container')

const { firstElementChild: area } = container

const step = .4
let _transformation = {}

const listenEvents = () => {

  let unzoom

  container.addEventListener('wheel', async ({ pageX, pageY, deltaY, deltaX }) => {

    if (unzoom)
      return

    const { style } = area
    const { transition = null } = style

    style.transition = 'none'
    await waitNextTick()

    // Normalize to deltaX in case shift modifier is used on Mac
    const delta = deltaY === 0 && deltaX ? deltaX : deltaY

    await zoom({
      x: pageX,
      y: pageY,
      ratio: Math.exp(((delta < 0 ? 1 : -1) * step) / 3),
    })

    style.transition = transition
  })

  container.addEventListener('mousemove', async event => {

    if (unzoom || event.buttons !== 1)
      return

    event.preventDefault()

    const { style } = area
    const { transition = null } = style

    style.transition = 'none'
    await waitNextTick()

    await panBy({
      deltaX: event.movementX,
      deltaY: event.movementY,
    })

    style.transition = transition
  })

  container.addEventListener('dblclick', async ({ target }) => {

    if (unzoom)
      return unzoom()

    unzoom = async () => {

      Object.assign(_transformation, await transformArea())
      unzoom = undefined
    }

    const {
      clientWidth: containerWidth,
    } = container

    const {
      offsetTop,
      offsetLeft,
    } = target

    const { width } = target.getBoundingClientRect()

    const ratio = containerWidth / width
    const scale = _transformation.scale * ratio

    Object.assign(_transformation, await transformAreaOrigin({
      ..._transformation,
      newOriginX: 0,
      newOriginY: 0,
    }))

    Object.assign(_transformation, await transformArea({
      ..._transformation,
      scale,
      translateX: -(offsetLeft * scale),
      translateY: -(offsetTop * scale),
    }))
  })
}


export const transformArea = transform(area)
export const transformAreaOrigin = transformOrigin(area)

const { zoom } = Zoomable({
  element: area,
  transformation: _transformation,
  transformElement: transformArea,
  transformElementOrigin: transformAreaOrigin,
})

const { panBy } = Pannable({
  element: area,
  transformation: _transformation,
  transformElement: transformArea,
  transformElementOrigin: transformAreaOrigin,
})

// eslint-disable-next-line no-unused-vars
const start1 = async () => {

  Object.assign(_transformation, await transformArea({ animated: false }))

  Object.assign(_transformation, await transformArea({
    originX: 50,
    originY: 50,
    translateX: 100,
    translateY: 100,
    scale: 1.752,
  }))

  Object.assign(_transformation, await transformAreaOrigin({
    ..._transformation,
    newOriginX: 0,
    newOriginY: 100,
  }))

  Object.assign(_transformation, await transformArea({
    ..._transformation,
    scale: 2,
  }))

  listenEvents()
}

// eslint-disable-next-line no-unused-vars
const start2 = async () => {

  Object.assign(_transformation, await transformArea({ animated: false }))

  await zoom({
    x: 400,
    y: 200,
    ratio: .25,
  })

  await zoom({
    x: 400,
    y: 200,
    ratio: 2,
  })

  await zoom({
    x: 400,
    y: 200,
    ratio: 2,
  })

  listenEvents()
}


// eslint-disable-next-line no-unused-vars
const start = async () => {

  Object.assign(_transformation, await transformArea({ animated: false }))

  listenEvents()
}

start2()