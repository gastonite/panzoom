import { transform } from './transform.js'





export const Pannable = ({
  element,
  transformation = {},
  transformElement = transform(element),
}) => {

  const panBy = async ({ deltaX, deltaY }) => {

    Object.assign(transformation, await transformElement({
      ...transformation,
      translateX: transformation.translateX + deltaX,
      translateY: transformation.translateY + deltaY,
      animated: false
    }))
  }

  return {
    panBy
  }
}
